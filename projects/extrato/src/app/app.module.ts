import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {createCustomElement} from '@angular/elements';
import {ExtratoListComponent} from './extrato-list/extrato-list.component';
import {RouterModule} from '@angular/router';
/* nome para definir o micro frontend extrato*/
const MICRO_APP_EXTRATO = 'micro-app-extrato';

@NgModule({
  declarations: [
    AppComponent,
    ExtratoListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'extrato',
        component: ExtratoListComponent
      }
    ], {useHash: true})
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class AppModule {
  constructor(private injector: Injector) {

  }

  ngDoBootstrap() {
    const element = createCustomElement(AppComponent, {injector: this.injector});
    customElements.define(MICRO_APP_EXTRATO, element);
  }
}
