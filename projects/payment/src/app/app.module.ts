import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {createCustomElement} from '@angular/elements';
import {RouterModule} from '@angular/router';
import {PaymentListComponent} from './payment-list/payment-list.component';
import {ExtratoListComponent} from '../../../extrato/src/app/extrato-list/extrato-list.component';

/* nome para definir o micro frontend extrato*/
const MICRO_APP_PAYMENT = 'micro-app-payment';


@NgModule({
  declarations: [
    AppComponent,
    PaymentListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'pagamento',
        component: PaymentListComponent
      }
    ], {useHash: true})
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: []
})
export class AppModule {
  constructor(private injector: Injector) {

  }

  ngDoBootstrap() {
    const element = createCustomElement(AppComponent, {injector: this.injector});
    customElements.define(MICRO_APP_PAYMENT, element);
  }


}
