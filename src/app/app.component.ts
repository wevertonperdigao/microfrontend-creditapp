import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'creditAppMaster';


  ngOnInit() {
    const scriptExtrato = document.createElement('script');
    scriptExtrato.src = 'http://localhost:8080/extrato/main-es2015.js';
    document.body.appendChild(scriptExtrato);

    const scriptPayment = document.createElement('script');
    scriptPayment.src = 'http://localhost:8080/payment/main-es2015.js';
    document.body.appendChild(scriptPayment);

  }

}
